///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ summation 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author  declan campbell < declanc@hawaii.edu>
/// @date    11_01_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[] ) {
   int n = atoi( argv[1] );
   //printf("value of n is %d \n", n); used for debugging to make sure n was recieved properly
   
   int sum = 0; //declare variable sum
   
   for (int i = 1; i <= n; i++){  
      sum = sum + i; //algorithm given was sum = sum +i
   
   //printf(" %d\n", sum); used for debuggging to see the value being printed while summing
   }

   printf("The sum of the digits from 1 to %d is %d \n", n, sum);  //print the final sum
   
   return 0;
}
